$(document).ready(function () {
    // TODO: Your code here
    for (let i = 0; i < 7; i++) {
        let backgroundCheck = $('#radio-background' + i);
        backgroundCheck.change(function () {
            // let backgroundDiv =  $('#background');
            // console.log(backgroundDiv[0].src);
            //   backgroundDiv.src = "../images/background"+i+".jpg";
            let ext = '.jpg';
            if (i == 6) {
                ext = '.gif';
            }
            $('#background').attr('src', "../images/background" + i + ext);
        })
    }
    for (let i = 0; i < 9; i++) {
        $('#dolphin' + i).hide();
        let imageChange = $('#check-dolphin' + i);
        if (imageChange.is(':checked')) {
            $('#dolphin' + i).show();
        }
    }

    for (let i = 0; i < 9; i++) {
        let imageChange = $('#check-dolphin' + i);
        imageChange.change(function () {
            if (imageChange.is(':checked')) {
                $('#dolphin' + i).show();
                console.log('test');
                // imageChange.change(function(){
                // imageChange.attr('src',"../images/dolphin"+i+".png");
            } else {
                $('#dolphin' + i).hide();
            }
        });
    }
    let slider = $('#vertical-control');
    slider.on('input change', function () {
        if ($(this).val() > 0) { }
        let d = $('.dolphin').css('transform', 'translateY('+$(this).val()+'px)');

    })
    let sliderH =$('#horizontal-control');
    sliderH.on('input change', function(){
        if ($(this).val() > 0) { }
        let d = $('.dolphin').css('transform', 'translateX('+$(this).val()+'px)');
    })
    let sliderSize =$('#size-control');
    sliderSize.on('input change', function(){
        if ($(this).val() > 0) { }
        let d = $('.dolphin').css('transform', 'scale('+(1 + $(this).val()*0.01)+')');
    })
});
